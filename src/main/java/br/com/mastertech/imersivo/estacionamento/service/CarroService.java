package br.com.mastertech.imersivo.estacionamento.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mastertech.imersivo.estacionamento.exception.CarroComEntradaDuplicadaException;
import br.com.mastertech.imersivo.estacionamento.exception.CarroSemEntradaException;
import br.com.mastertech.imersivo.estacionamento.model.Carro;
import br.com.mastertech.imersivo.estacionamento.model.Estado;
import br.com.mastertech.imersivo.estacionamento.repository.CarroRepository;

@Service
public class CarroService {

	@Autowired
	private CarroRepository carroRepository;
	
	public void registrarEntrada(String placa, Date data) {
		Carro carro = carroRepository.findByPlacaAndEstado(placa, Estado.PENDENTE);
		
		if(carro != null) {
			throw new CarroComEntradaDuplicadaException();
		}
		
		carro = new Carro();
		carro.setPlaca(placa);
		carro.setEstado(Estado.PENDENTE);
		carro.setEntrada(data);
		carroRepository.save(carro);//TODO exception
	}
	
	public Carro registrarSaida(String placa, Date data) {
		Carro carro = carroRepository.findByPlacaAndEstado(placa, Estado.PENDENTE);
		
		if(carro == null) {
			throw new CarroSemEntradaException();
		}
		
		carro.setSaida(data);
		carro.setEstado(Estado.PAGO);
		
		double valorFinal = calculaValor(carro.getEntrada(), carro.getSaida());
		carro.setValorFinal(valorFinal);

		return carroRepository.save(carro);
	}

	private double calculaValor(Date entrada, Date saida) {
		int horas = calculaHoras(entrada, saida);
		
		double valorFinal = 0;
		
		if(horas >= 24) {
			int dias = horas / 24;
			if(horas % 24 != 0)
				dias += 1;
			
			valorFinal += 25 * dias;
			horas -= dias * 24;
		}
		
		if(horas > 0) {
			if(horas >= 6) {
				valorFinal += 25;
			}else {
				valorFinal += 10;
				valorFinal += (horas -1) * 3;
			}
		}
		
		return valorFinal;
	}

	private int calculaHoras(Date entrada, Date saida) {
		long diff = saida.getTime() - entrada.getTime();
		long hora = 60*60*1000;
		
		int horas = (int) (diff / hora);
		
		if(diff % hora != 0) {
			horas += 1;
		}

		return horas;
	}
	
	
}
