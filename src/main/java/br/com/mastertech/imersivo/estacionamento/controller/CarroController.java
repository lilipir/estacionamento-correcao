package br.com.mastertech.imersivo.estacionamento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mastertech.imersivo.estacionamento.dto.DataDTO;
import br.com.mastertech.imersivo.estacionamento.model.Carro;
import br.com.mastertech.imersivo.estacionamento.service.CarroService;

@RestController
@RequestMapping("/carro")
public class CarroController {

	@Autowired
	private CarroService carroService;
	
	@PostMapping("/{placa}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void registrarEntrada(@PathVariable String placa, @RequestBody DataDTO dataDTO) {
		carroService.registrarEntrada(placa, dataDTO.getData());
	}
	
	@PatchMapping("/{placa}/saida")
	public Carro registrarSaida(@PathVariable String placa, @RequestBody DataDTO dataDTO) {
		return carroService.registrarSaida(placa, dataDTO.getData());
	}
	
}
